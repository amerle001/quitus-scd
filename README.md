# Quitus-SCD
Application pour délivrer automatiquement un quitus aux étudiants qui en font la demande.

**Serveur** : nodejs + express.
**Client** : React.

## Utilisation
L'étudiant entre son code-barre de carte Aquipass ainsi que sa date de naissance pour s'identifier. 
Si il est en règle (plus de prêts sur sa carte), l'application lui délivre un quitus à son nom en pdf. Son compte est gelé dans Alma.
S'il lui reste des documents sur sa carte, leur liste s'affiche pour rappel.

## Installation
```bash
# Installation des dépendances pour le serveur
npm install

# Installation des dépendances pour la partie client
npm run client-install

# Lancer le serveur et le client en même temps
npm run dev

# Le serveur fonctionne sur http://localhost:5000 et le client sur http://localhost:3000
```
