const express = require("express");
const path = require("path");

const app = express();
app.disable("x-powered-by"); // sécurité contre fingerprinting

// ----------------- Middlewares ---------------------
// BodyParser est inclus dans Express
app.use(express.json());

//------------- partie express : serveur et routage ----------

app.use("/api/block_user", require("./routes/api/blockUser"));
app.use("/api/get_user_brief", require("./routes/api/getUserBrief"));
app.use("/api/check_credentials", require("./routes/api/checkCredentials"));
app.use("/api/get_loans", require("./routes/api/getLoans"));

app.use(express.static("client/build"));

app.get("*", (req, res) => {
  res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
});

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
