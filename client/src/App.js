import "./App.css";
import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import Accueil from "./components/Accueil";

function App() {
  return (
    <main>
      <div>
        <Accueil />
      </div>
    </main>
  );
}

export default App;
