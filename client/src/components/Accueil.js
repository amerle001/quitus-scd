import React, { useState } from "react";
import axios from "axios";
import UPPA_logo from "../img/logo_uppa.png";
import App_logo from "../img/app_logo.jpg";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Quitus from "./Quitus";
import Footer from "./Footer";

const postRequest = async (barcode, ddn) => {
  const response = await axios.post("/api/check_credentials", {
    user: barcode,
    ddn,
  });
  return response.data;
};

export default function Accueil() {
  const [barcode, setBarcode] = useState(null);
  const [ddn, setDdn] = useState(null);
  const [credentialCheckSuccess, setCredentialCheckSuccess] = useState(null);
  const [soumis, setSoumis] = useState(false);
  const [quitusRequestStatus, setQuitusRequestStatus] = useState("idle");
  const canSubmit = barcode && ddn && quitusRequestStatus === "idle";

  const onSubmit = async (e) => {
    e.preventDefault();
    if (canSubmit) {
      try {
        setQuitusRequestStatus("pending");
        const response = await postRequest(barcode, ddn);
        setCredentialCheckSuccess(response);
        setSoumis(true);
      } catch (error) {
        console.log(error);
      } finally {
        setQuitusRequestStatus("idle");
        setBarcode(null);
        setDdn(null);
      }
    }
  };

  const erreur =
    credentialCheckSuccess === false ? (
      <p>Les informations entrées n'ont pas permis de vous identifier.</p>
    ) : null;

  const reussite =
    credentialCheckSuccess === true ? <Quitus user={barcode} /> : null;

  return (
    <div>
      <header>
        <img src={UPPA_logo} className="UPPA-logo" alt="UPPA-logo" />
      </header>

      <div className="conteneur">
        <img src={App_logo} className="App-logo" alt="App-logo" />
        <p>
          <a className="App-link" href="https://bibliotheques.univ-pau.fr/">
            Service commun de la documentation
          </a>
        </p>
        <p>Bienvenue sur l'application de délivrance du quitus.</p>
        {soumis ? (
          <div>
            {erreur}
            {reussite}
          </div>
        ) : (
          <div className="form">
            <Form>
              <Form.Group>
                <Form.Label>
                  Entrez le code-barre de votre carte Aquipass
                </Form.Label>
                <Form.Control
                  type="number"
                  placeholder="code-barre"
                  onChange={(e) => setBarcode(e.target.value)}
                />
              </Form.Group>
              <Form.Group>
                <Form.Label>Entrez votre date de naissance :</Form.Label>
                <Form.Control
                  type="date"
                  placeholder="jj/mm/aaaa"
                  onChange={(e) => setDdn(e.target.value)}
                />
              </Form.Group>
              <Button
                className="bouton"
                variant="primary"
                type="submit"
                disabled={!canSubmit}
                onClick={(e) => onSubmit(e)}
              >
                Soumettre
              </Button>
            </Form>
          </div>
        )}
      </div>
      <Footer />
    </div>
  );
}
