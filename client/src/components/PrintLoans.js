import React from "react";

export default function PrintLoans(props) {
  return (
    <div>
      <table className="table table-dark">
        <thead>
          <tr>
            <th>Documents encore présents sur votre carte.</th>
          </tr>
          {props.loans.map((doc) => {
            return (
              <tr key={doc.item_barcode}>
                <td>{doc.title}</td>
              </tr>
            );
          })}
        </thead>
      </table>
    </div>
  );
}
