import React from "react";

export default function Footer() {
  return (
    <div>
      <p className="footer">
        <a href="https://git.univ-pau.fr/amerle001/quitus-scd">Quitus SCD</a>{" "}
        {new Date().getFullYear()} -- v 0.10
      </p>
    </div>
  );
}
