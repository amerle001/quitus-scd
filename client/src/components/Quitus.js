import React, { useEffect, useState } from "react";
import PrintLoans from "./PrintLoans";
import { generateQuitus } from "../lib/quitus";
import axios from "axios";

const oracleDuQuitus = (nombrePrets, loans) => {
  if (nombrePrets === 0) return <p>Quitus accordé</p>;
  if (nombrePrets === null) return null;

  if (nombrePrets > 0) {
    return (
      <div>
        <p>
          Vous avez encore {nombrePrets} {nombrePrets === 1 ? "prêt" : "prêts"}{" "}
          en cours. Quitus non accordé.
        </p>
        <br />
        <PrintLoans loans={loans} />
      </div>
    );
  }
};

// Génère un quitus et bloque l'usager, ou affiche la liste de documents à rendre
export default function Quitus(props) {
  const [loans, setLoans] = useState([]);
  const [nombrePrets, setNombrePrets] = useState(null);
  const [fetchDataStatus, setFetchDataStatus] = useState("idle");

  const fetchData = async (user) => {
    const response = await axios.post("/api/get_loans", user);
    setLoans(response.data.item_loan);

    // Si l'usager a encore des docs sur sa carte
    if (response.data.item_loan) {
      const nbreDocs = response.data.item_loan.length;
      setNombrePrets(nbreDocs);
      console.log("nbreDocs " + nbreDocs);
    }

    // Si l'usager n'a plus de prêts en cours
    if ((response.data.item_loan ?? []).length === 0) {
      console.log("je bloque l'utilisateur");
      setNombrePrets(0);
      console.log("nbreDocs " + 0);
      blockUserInAlmaAndEditQuitus({ user: props.user });
    }
  };

  const blockUserInAlmaAndEditQuitus = async (user) => {
    axios.post("/api/block_user", user).then((res) => res.data);

    const nomPrenom = await axios.post("/api/get_user_brief", user);
    generateQuitus(nomPrenom.data);
  };

  useEffect(() => {
    if (props.user && fetchDataStatus === "idle") {
      try {
        setFetchDataStatus("pending");
        fetchData({ user: props.user });
      } catch (error) {
        console.log("echec de la récupération des données : " + error);
      } finally {
        setFetchDataStatus("idle");
      }
    }
    return () => {
      console.log("cleanup");
    };
  }, []);

  return <div>{oracleDuQuitus(nombrePrets, loans)}</div>;
}
