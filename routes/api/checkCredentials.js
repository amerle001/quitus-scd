// configuration, variables d'environnement
require("dotenv").config({ path: "./config/.env" });

const express = require("express");
const router = express.Router();
const fetch = require("node-fetch");
require("dotenv").config({ path: "../../config/.env" });
const apiKey = process.env.API_KEY;
const format = "&format=json"; // à accoler à l'url de requête, sinon elle renvoit de l'xml par défaut

// @route   POST api/checkCredentials
// @desc    Vérifie si codebarre et ddn sont ok et renvoie un booléen
// @access  PUBLIC

// CheckCredentials(this.state.barcode, this.state.ddn)
router.post("/", (req, res) => {
  const { user, ddn } = req.body;
  const ddn_saisie = new Date(ddn);

  const url = `https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/${user}?user_id_type=all_unique&view=full&expand=none&apikey=${apiKey}${format}`;

  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      const birth_date = new Date(data.birth_date);
      console.log("birth date : " + birth_date);
      if (birth_date.toString() == ddn_saisie.toString()) {
        console.log("Les informations concordent");
        res.send(true);
      } else {
        console.log("Erreur : les informations ne concordent pas.");
        res.send(false);
      }
    });
});

module.exports = router;
