const express = require("express");
const axios = require("axios");
const router = express.Router();
const fetch = require("node-fetch");
require("dotenv").config({ path: "./config/.env" });
const apiKey = process.env.API_KEY;
const format = "&format=json"; // à accoler à l'url de requête, sinon elle renvoit de l'xml par défaut

async function blockUser(user) {
  // appel API ALMA GET = recupère objet user
  // change les objets block_type, block_description, etc
  // appel API ALMA PUT avec le nouvel objet complet
  const url = `https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/${user}?user_id_type=all_unique&view=full&expand=none&apikey=${apiKey}${format}`;

  const date = new Date();

  // Array d'objet à assigner à l'objet user pour générer un blocage
  const blocageJson = [
    {
      block_type: {
        value: "GENERAL",
        desc: "General",
      },
      block_description: {
        value: "03-LOCAL",
        desc: "Quitus délivré(LOCAL)",
      },
      block_status: "ACTIVE",
      block_note: "Compte bloqué suite à délivrance du quitus.",
      created_by: "QUITUS_SCD_APP",
      created_date: date,
      segment_type: "Internal",
    },
  ];

  // On crée un nouvel état à partir de l'ancien, et on l'envoie avec PUT
  const response = await fetch(url);

  const userBefore = await response.json();
  let userAfter = await userBefore;
  userAfter.user_block = blocageJson;

  await axios.put(url, userAfter).catch((err) => console.log(err));
}

// @route   POST api/blockUser
// @desc    Bloque utilisateur dans alma après délivrance du quitus
// @access  PUBLIC

router.post("/", (req, res) => {
  const { user } = req.body;
  (async () => {
    await blockUser(user).catch((err) => console.log(err));
  })();
  res.json();
});

module.exports = router;
