const express = require("express");
const router = express.Router();
const fetch = require("node-fetch");
require("dotenv").config({ path: "../../config/.env" });
const apiKey = process.env.API_KEY;
const format = "&format=json"; // à accoler à l'url de requête, sinon elle renvoit de l'xml par défaut

// @route   POST api/getLoans
// @desc    Renvoie les prêts de l'usager
// @access  PUBLIC

router.post("/", (req, res) => {
  (async () => {
    const { user } = req.body;
    const url = `https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/${user}/loans?user_id_type=all_unique&view=full&expand=none&apikey=${apiKey}${format}`;
    const response = await fetch(url);
    const loans = await response.json();
    res.json(loans);
  })();
});

module.exports = router;
