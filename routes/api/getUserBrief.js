const express = require("express");
const router = express.Router();
const fetch = require("node-fetch");
require("dotenv").config({ path: "./config/.env" });
const apiKey = process.env.API_KEY;
const format = "&format=json"; // à accoler à l'url de requête, sinon elle renvoit de l'xml par défaut

// @route   GET api/getUserBrief
// @desc    Renvoie une fiche user abrégée
// @access  PUBLIC

router.post("/", (req, res) => {
  (async () => {
    const barcode = req.body.user;

    const url = `https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/${barcode}?user_id_type=all_unique&view=brief&expand=none&apikey=${apiKey}${format}`;
    const response = await fetch(url);
    const data = await response.json();

    const prenom =
      data.first_name[0].toUpperCase() + data.first_name.slice(1).toLowerCase();

    const user = {
      nom: data.last_name.toUpperCase(),
      prenom: prenom,
    };
    res.json(user);
  })();
});

module.exports = router;
